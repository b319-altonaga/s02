const { factorial, div_check } = require("../src/util.js");
const { expect, assert } = require("chai");

// Test Suite [test_fun_factorials]
describe("test_fun_factorials", () => {
  it("test_fun_factorial_5!_is_120", () => {
    expect(factorial(5)).to.equal(120);
  });

  it("test_fun_factorial_1!_is_1", () => {
    assert.equal(factorial(1), 1);
  });

  it("test_fun_factorial_0!_is_1", () => {
    assert.equal(factorial(0), 1);
  });

  it("test_fun_factorial_4!_is_24", () => {
    const product = factorial(4);
    expect(product).to.equal(24);
  });
  it("test_fun_factorial_10!_is_3628800", () => {
    assert((factorial(10), 3628800));
  });
});

// Test Suite [test_divisibility_by_5_or_7]
describe("test_divisibility_by_5_or_7", () => {
  it("test_100_is_divisible_by_5", () => {
    assert(div_check(100), true);
  });

  it("test_49_is_divisible_by_7", () => {
    assert(div_check(49), true);
  });

  it("test_30_is_divisible_by_5", () => {
    assert(div_check(30), true);
  });

  it("test_56_is_divisible_by_7", () => {
    assert(div_check(56), true);
  });
});
