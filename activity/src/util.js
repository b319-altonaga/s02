function factorial(n) {
  if (typeof n !== "number" || n < 0) return undefined;
  if (n === 0 || n === 1) return 1;
  return n * factorial(n - 1);
}

function div_check(n) {
  if (n % 5 === 0) return true;
  if (n % 7 === 0) return true;
  return false;
}

module.exports = {
  factorial: factorial,
  div_check: div_check,
};
