const { factorial } = require("../src/util.js");
const { expect, assert } = require("chai");

it("test_fun_factorial_5!_is_120", () => {
  expect(factorial(5)).to.equal(120);
});

it("test_fun_factorial_1!_is_1", () => {
  assert.equal(factorial(1), 1);
});

it("test_fun_factorial_-1!_is_undefined", () => {
  const product = factorial(-1);
  expect(product).to.equal(undefined);
});

describe("test_fun_factorials", () => {
  it("test_fun_factorial_5!_is_120", () => {
    expect(factorial(5)).to.equal(120);
  });

  it("test_fun_factorial_n!_is_string", () => {
    const product = factorial("String!");
    assert.equal(product, undefined);
  });
});
